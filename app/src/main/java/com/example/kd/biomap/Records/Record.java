package com.example.kd.biomap.Records;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.kd.biomap.Globals.Globals;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by deontp on 2016/05/23.
 */



@DatabaseTable(tableName = "record")
public class Record implements Parcelable, Serializable{

    @DatabaseField
    public String RECORD_ID;

    @DatabaseField(generatedId = true, canBeNull = false)
    public int _id;

    @DatabaseField
    public String FULL_NAME;

    @DatabaseField
    public String EMAIL;

    @DatabaseField
    public String COUNTRY;

    @DatabaseField
    public String PROVINCE;

    @DatabaseField
    public String NEAREST_TOWN;

    @DatabaseField
    public String LATITUDE;

    @DatabaseField
    public String LONGITUDE;

    @DatabaseField
    public String ENVIRONMENT_TYPE;

    @DatabaseField
    public String ENVIRONMENT_DESCRIPTION;

    @DatabaseField
    public String PROJECT_TYPE;

    @DatabaseField
    public String IDENTIFICATION_TYPE;

    @DatabaseField
    public String GROWING_FROM;

    @DatabaseField
    public String GROWING;

    @DatabaseField
    public String SCENT_DESCRIPTION;

    @DatabaseField
    public String DATE;

    @DatabaseField
    public String TIME;

    @DatabaseField
    public Boolean HAS_FLOWERS, HAS_FRUIT, HAS_SCENT;

    @DatabaseField
    public Integer ADU_NUMBER, AMOUNT_SPOTTED;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    public ArrayList<String> IMAGE_PATHS;

//   public LatLng COORDS;

    public Record (){

    }

    public Record(String ID) {
        SimpleDateFormat time = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        this.FULL_NAME = Globals.FULL_NAME;
        this.EMAIL = Globals.EMAIL_ADDRESS;
        this.ADU_NUMBER = Globals.ADU_NUMBER;
        this.COUNTRY = Globals.COUNTRY;
        this.PROVINCE = Globals.PROVINCE;
        this.DATE = date.format(new Date());
        this.TIME = time.format(new Date());
        this.HAS_FLOWERS = false;
        this.HAS_FRUIT = false;
        this.HAS_SCENT = false;
        this.AMOUNT_SPOTTED = 0;
        this.RECORD_ID = ID;
        this.IMAGE_PATHS = new ArrayList<>();
    }

    public int getImageCount (){

        return IMAGE_PATHS.size();
    }

    public void addImage(String path){

        IMAGE_PATHS.add(path);
    }

    public void removeImage(Integer index){

        this.IMAGE_PATHS.remove(index);
    }

    public static final Parcelable.Creator<Record> CREATOR = new Parcelable.Creator<Record>() {
        public Record createFromParcel(Parcel in) {

            return new Record(in);
        }

        public Record[] newArray(int size) {

            return new Record[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_id);
        dest.writeString(RECORD_ID);
        dest.writeString(FULL_NAME);
        dest.writeString(EMAIL);
        dest.writeString(COUNTRY);
        dest.writeString(PROVINCE);
        dest.writeString(NEAREST_TOWN);
        dest.writeString(LATITUDE);
        dest.writeString(LONGITUDE);
        dest.writeString(ENVIRONMENT_TYPE);
        dest.writeString(ENVIRONMENT_DESCRIPTION);
        dest.writeString(PROJECT_TYPE);
        dest.writeString(IDENTIFICATION_TYPE);
        dest.writeString(GROWING_FROM);
        dest.writeString(GROWING);
        dest.writeString(SCENT_DESCRIPTION);
        dest.writeString(DATE);
        dest.writeString(TIME);
        dest.writeString(HAS_FLOWERS.toString());
        dest.writeString(HAS_FRUIT.toString());
        dest.writeString(HAS_SCENT.toString());
        dest.writeString(ADU_NUMBER.toString());
        dest.writeString(AMOUNT_SPOTTED.toString());
        dest.writeString(myArrayToString(IMAGE_PATHS));
    }

    private String myArrayToString(ArrayList<String> input){
        String result = input.toString();
        return result.substring(1,result.length()-1);
    }

    @Override
    public int describeContents() {

        return 0;
    }

    public Record(Parcel in){
        _id = in.readInt();
        RECORD_ID = in.readString();
        FULL_NAME = in.readString();
        EMAIL = in.readString();
        COUNTRY = in.readString();
        PROVINCE = in.readString();
        NEAREST_TOWN = in.readString();
        LATITUDE = in.readString();
        LONGITUDE = in.readString();
        ENVIRONMENT_TYPE = in.readString();
        ENVIRONMENT_DESCRIPTION = in.readString();
        PROJECT_TYPE = in.readString();
        IDENTIFICATION_TYPE = in.readString();
        GROWING_FROM = in.readString();
        GROWING = in.readString();
        SCENT_DESCRIPTION = in.readString();
        DATE = in.readString();
        TIME = in.readString();
        HAS_FLOWERS = Boolean.parseBoolean(in.readString());
        HAS_FRUIT = Boolean.parseBoolean(in.readString());
        HAS_SCENT = Boolean.parseBoolean(in.readString());
        ADU_NUMBER = Integer.parseInt(in.readString());
        AMOUNT_SPOTTED = Integer.parseInt(in.readString());
        String [] temp = in.readString().split(",");
        if(temp[0].equals(""))
            IMAGE_PATHS = new ArrayList<>();
        else
            IMAGE_PATHS = new ArrayList<> (Arrays.asList(temp));
    }

    @Override
    public String toString() {
        return "Record{" +
                "id='" + RECORD_ID + '\'' +
                ", FULL_NAME='" + FULL_NAME + '\'' +
                ", COUNTRY='" + COUNTRY + '\'' +
                ", PROVINCE='" + PROVINCE + '\'' +
                ", NEAREST_TOWN='" + NEAREST_TOWN + '\'' +
                ", LATITUDE='" + LATITUDE + '\'' +
                ", LONGITUDE='" + LONGITUDE + '\'' +
                ", ENVIRONMENT_TYPE='" + ENVIRONMENT_TYPE + '\'' +
                ", ENVIRONMENT_DESCRIPTION='" + ENVIRONMENT_DESCRIPTION + '\'' +
                ", PROJECT_TYPE='" + PROJECT_TYPE + '\'' +
                ", IDENTIFICATION_TYPE='" + IDENTIFICATION_TYPE + '\'' +
                ", GROWING_FROM='" + GROWING_FROM + '\'' +
                ", GROWING='" + GROWING + '\'' +
                ", SCENT_DESCRIPTION='" + SCENT_DESCRIPTION + '\'' +
                ", DATE='" + DATE + '\'' +
                ", TIME='" + TIME + '\'' +
                ", HAS_FLOWERS=" + HAS_FLOWERS +
                ", HAS_FRUIT=" + HAS_FRUIT +
                ", HAS_SCENT=" + HAS_SCENT +
                ", ADU_NUMBER=" + ADU_NUMBER +
                ", AMOUNT_SPOTTED=" + AMOUNT_SPOTTED +
                ", IMAGES=" + IMAGE_PATHS +
                '}';
    }
}
