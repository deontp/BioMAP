package com.example.kd.biomap.Globals;

/**
 * Created by deontp on 2016/05/23.
 */
public class Globals {

    // Settings Shared Preferences
    public static final String SETTINGS_PREF = "SettingsPREF";
    public static final String SETTINGS_COUNTRY = "country";
    public static final String SETTINGS_PROVINCE = "province";
    public static final String SETTINGS_LANGUAGE = "language";

    // Share Preferences entry names
    public  static final String PREFS_FILE_REGISTRATION_DETAILS = "registration_details";
    public static final String PREFS_ENTRY_REGISTERED = "registered";
    public static final String PREFS_ENTRY_FULL_NAME = "full_name";
    public static final String PREFS_ENTRY_EMAIL_ADDRESS = "email_address";
    public static final String PREFS_ENTRY_ADU_NUMBER = "adu_number";
    public static final String PREFS_ENTRY_TOKEN = "user_token";

    // Variables
    public static final String ADU_WEBSITE = "http://www.adu.org.za/register.php?project=vmus";
    public static final String NA = "N/A";

    // User registered details
    public static String FULL_NAME;
    public static String EMAIL_ADDRESS;
    public static Integer ADU_NUMBER;
    public static String COUNTRY;
    public static String PROVINCE;
    public static String LANGUAGE;
    public static String TOKEN;

    private static Globals ourInstance = new Globals();

    public static Globals getInstance() {
        return ourInstance;
    }

    private Globals() {
    }
}
