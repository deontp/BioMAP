package com.example.kd.biomap;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcelable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.kd.biomap.Records.Record;

public class ManualCoordinatesActivity extends Activity {

    Record currentRecord;
    Spinner latS;
    Spinner longS;
    EditText latDeg;
    EditText latMin;
    EditText latSec;
    EditText longDeg;
    EditText longMin;
    EditText longSec;

    String latitude;
    String longitude;

    //references to arrays stored in strings.xml
    String[] latitudeAr;
    String[] longitudeAr;

//Still need to implement shared Preferences to record coordinates
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_coordinates);


        currentRecord = getIntent().getParcelableExtra(Record.class.toString());

        //sets up the spinners according to the arrays in strings.xml
        latS = (Spinner) findViewById(R.id.Latspinner);
        latitudeAr = getResources().getStringArray(R.array.latitude_array);

        longS = (Spinner) findViewById(R.id.Longspinner);
        longitudeAr = getResources().getStringArray(R.array.longitude_array);

        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, latitudeAr);

        ArrayAdapter<String> LongAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, longitudeAr);

        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        latS.setAdapter(SpinnerAdapter);

        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        longS.setAdapter(LongAdapter);


        //set up editText stuff, to save coordinates
        //latitude;
        latDeg = (EditText) findViewById(R.id.LatDegEditText);
        latMin = (EditText) findViewById(R.id.LatMinEditText);
        latSec = (EditText) findViewById(R.id.LatSecEditText);
        //longitude;
        longDeg = (EditText) findViewById(R.id.LoDegreesEditText);
        longMin =  (EditText) findViewById(R.id.LoMinEditText);
        longSec = (EditText) findViewById(R.id.LoSecondsEditText);


       // Toolbar myToolbar = (Toolbar) findViewById(R.id.manual_coordinates_toolbar_top); // creates the toolbar
        // myToolbar.setTitle("Coordinates"); // gives it a title
       // setSupportActionBar(myToolbar); // sets it as action bar
    }

    public void onClick(View view){
        //save values to latitude and longitude and push to parceled record.
        String lati=longS.getSelectedItem().toString();
        String longi =latS.getSelectedItem().toString();

        latitude = (latDeg.getText().toString()) + "° " + (latMin.getText().toString()) + "' " + (latSec.getText().toString()) + "'' " + lati.substring(0,1);
        longitude = (longDeg.getText().toString()) + "° " + (longMin.getText().toString()) + "' " + (longSec.getText().toString()) + "'' " + longi.substring(0,1);

        currentRecord.LATITUDE = latitude;
        currentRecord.LONGITUDE = longitude;


        Intent recIntent = new Intent(getApplicationContext(), RecordActivity.class);
        recIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(recIntent);
    }
}
