package com.example.kd.biomap;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.kd.biomap.Records.Record;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {


    private static LocationManager locationManager;
    private static SupportMapFragment mapFragment;
    private static Location location;
    private static TextView tvCurrentLocation;
    private static double selectedLatitude;
    private static double selectedLongitude;
    private static LatLng latLng;
    private static Marker mark;

    Record currentRecord;

    private static GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.map_toolbar_top); // creates the toolbar
        if (myToolbar != null) {
            myToolbar.setTitle("Map"); // gives it a title
            setSupportActionBar(myToolbar); // sets it as action bar
        }

        // get a reference to the map fragment
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tvCurrentLocation = (TextView) findViewById(R.id.tv_selected_location);
        currentRecord = getIntent().getParcelableExtra(Record.class.toString());
        //updateLocation();

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_done) {
            currentRecord.LATITUDE = String.valueOf(selectedLatitude);
            currentRecord.LONGITUDE = String.valueOf(selectedLongitude);
            Intent recIntent = new Intent(getApplicationContext(), RecordActivity.class);
            recIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
            startActivity(recIntent);
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener(){
            @Override
            public void onMapClick(LatLng point){

                if(mark != null){
                    mark.remove();
                }
                mark = googleMap.addMarker(new MarkerOptions().position(point).draggable(true).visible(true));

                //recording lat and long from latlong format
                selectedLatitude = mark.getPosition().latitude;
                selectedLongitude = mark.getPosition().longitude;
                //round up
                selectedLatitude = Math.round(selectedLatitude * 100.0 ) / 100.0;
                selectedLongitude = Math.round(selectedLongitude * 100.0 ) / 100.0;
                //currentRecord.LATITUDE = String.valueOf(selectedLatitude);
                //currentRecord.LONGITUDE = String.valueOf(selectedLongitude);

                tvCurrentLocation.setText("Latitude:" +  selectedLatitude  + " | Longitude:"+ selectedLongitude);
            }
        });
        try {
            googleMap.setMyLocationEnabled(true);
        }catch(SecurityException e){

        }
    }

//    public void updateLocation(){
//        // Setting latitude and longitude in the TextView tv_location
//        tvCurrentLocation.setText("Latitude:" +  mark.getPosition().latitude  + ", Longitude:"+ mark.getPosition().longitude);
//    }
}
