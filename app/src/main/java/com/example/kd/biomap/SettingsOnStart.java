package com.example.kd.biomap;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.kd.biomap.Globals.Globals;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class SettingsOnStart extends Activity {


    private String[] countries;
    private Spinner Cspinner;
    private int a;


    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_on_start);

        settings = getSharedPreferences(Globals.SETTINGS_PREF, 0);
        countries = getResources().getStringArray(R.array.countries_list);
        Cspinner = (Spinner) findViewById(R.id.Cspinner);

        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);

        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Cspinner.setAdapter(SpinnerAdapter);

        Cspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                a = Cspinner.getSelectedItemPosition();
                Cspinner.getSelectedItemPosition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void onClickSave(View view) {
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(Globals.SETTINGS_COUNTRY, countries[a]);

        editor.apply();

        Intent homeActivity = new Intent(getApplicationContext(), HomeActivity.class);
        homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
        startActivity(homeActivity);
    }
}
