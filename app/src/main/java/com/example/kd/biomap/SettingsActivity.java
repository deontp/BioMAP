package com.example.kd.biomap;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kd.biomap.Globals.Globals;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.Arrays;
import java.util.Locale;

public class SettingsActivity extends AppCompatActivity {

/*    public static final String SETTINGS_PREF = "SettingsPREF";
    public static final String SETTINGS_COUNTRY = "country";
    public static final String SETTINGS_PROVINCE = "province";
    public static final String SETTINGS_LANGUAGE = "language";
*/
    private int a;
    private int b;
    private int c;

    private String[] countries;
    private String[] provinces;
    private String[] languages;
    private Spinner Cspinner;
    private Spinner Pspinner;
    private Spinner Lspinner;
    private TextView Provinces;

    private static String country;
    private static String province;
    private static String language;

    SharedPreferences settings;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        settings = getSharedPreferences(Globals.SETTINGS_PREF, 0);
        country = settings.getString(Globals.SETTINGS_COUNTRY, "South Africa");
        province = settings.getString(Globals.SETTINGS_PROVINCE, "Limpopo");
        language = settings.getString(Globals.SETTINGS_LANGUAGE, "Afrikaans");


        countries = getResources().getStringArray(R.array.countries_list);
        provinces = getResources().getStringArray(R.array.province_list);
        languages = getResources().getStringArray(R.array.languages);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.settings_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Settings");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onBackPressed();
                }
            });
        }

        Cspinner = (Spinner) findViewById(R.id.Cspinner);
        Pspinner = (Spinner) findViewById(R.id.Pspinner);
        Lspinner = (Spinner) findViewById(R.id.Lspinner);
        Provinces = (TextView) findViewById(R.id.ProvinceLabel);

        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        ArrayAdapter<String> ProvAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, provinces);
        ArrayAdapter<String> LangAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, languages);


        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Cspinner.setAdapter(SpinnerAdapter);
        SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, provinces);
        Pspinner.setAdapter(ProvAdapter);
        SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, languages);
        Lspinner.setAdapter(LangAdapter);


        //Set the spinners default values based on the saved Shared Preference values
        String Csetter = settings.getString(Globals.SETTINGS_COUNTRY,"");
        if(Arrays.asList(countries).contains(Csetter))
        {
            int i = Arrays.asList(countries).indexOf(Csetter);
            Cspinner.setSelection(i);
        }

        String Psetter = settings.getString(Globals.SETTINGS_PROVINCE,"");
        if(Arrays.asList(provinces).contains(Psetter))
        {
            int i = Arrays.asList(provinces).indexOf(Psetter);
            Pspinner.setSelection(i);
        }

        String Lsetter = settings.getString(Globals.SETTINGS_LANGUAGE,"");
        if(Arrays.asList(languages).contains(Lsetter))
        {
            int i = Arrays.asList(languages).indexOf(Lsetter);
            Lspinner.setSelection(i);
        }

        Pspinner.setEnabled(false);
        Provinces.setEnabled(false);


        Cspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                a = Cspinner.getSelectedItemPosition();
                Cspinner.getSelectedItemPosition();

                if (a == 0) {//south africa is item 1 in countries array
                    Pspinner.setEnabled(true);
                    Provinces.setEnabled(true);
                }else{
                    Pspinner.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        Pspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                b = Pspinner.getSelectedItemPosition();
                Pspinner.getSelectedItemPosition();

                if (a == 0) {//south africa is item 1 in countries array
                    Pspinner.setEnabled(true);
                    Provinces.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        Lspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                c = Lspinner.getSelectedItemPosition();
                Lspinner.getSelectedItemPosition();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void toEnglish(View view){translateTo("en");}

    public void toXhosa(View view){translateTo("xh");}

    public void toAfrikaans (View view) {translateTo("af");}


    public void translateTo(String str) {
        Locale locale = new Locale (str);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
        recreate();
    }

    public void onClickSave(View view) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Globals.SETTINGS_COUNTRY, countries[a]);
        editor.putString(Globals.SETTINGS_PROVINCE, provinces[b]);
        editor.putString(Globals.SETTINGS_LANGUAGE, languages[c]);
        editor.apply();


        if(c==0){
            toEnglish(view);
        }
        if(c==1){
            toAfrikaans(view);
        }
        if(c==2){
            toXhosa(view);
        }

        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
        startActivity(intent);

    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Settings Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kd.biomap/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Settings Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kd.biomap/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
