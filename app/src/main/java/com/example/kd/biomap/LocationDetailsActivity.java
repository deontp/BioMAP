package com.example.kd.biomap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.ORMLlite.DatabaseHelper;
import com.example.kd.biomap.Records.Record;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Arrays;

public class LocationDetailsActivity extends AppCompatActivity {

    Record currentRecord;
    private static Spinner provinceSpinner, countrySpinner;
    private static EditText nearestTownET;

    private static TextView provinceLabel;
    private DatabaseHelper dbHelper = null;
    private Dao<Record, Integer> recordDao;
    String [] countries;
    String [] provinces;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);

        currentRecord = getIntent().getParcelableExtra(Record.class.toString());

        Toolbar myToolbar = (Toolbar) findViewById(R.id.location_details_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Location Details");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }

        countries = getResources().getStringArray(R.array.countries_list);
        provinces = getResources().getStringArray(R.array.province_list);

        provinceLabel = (TextView) findViewById(R.id.label_province);
        countrySpinner = (Spinner) findViewById(R.id.spinner_country);
        provinceSpinner = (Spinner) findViewById(R.id.spinner_province);
        nearestTownET = (EditText) findViewById(R.id.editText_nearest_town);

        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, countries);
        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countrySpinner.setAdapter(SpinnerAdapter);
        SpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, provinces);
        provinceSpinner.setAdapter(SpinnerAdapter);



        countrySpinner.setSelection(Arrays.binarySearch(countries, Globals.COUNTRY));
        provinceSpinner.setSelection(Arrays.binarySearch(provinces, Globals.PROVINCE));
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
                int selectedPosition = countrySpinner.getSelectedItemPosition();
//                countrySpinner.getSelectedItemPosition();

                if(selectedPosition == 1){//south africa is item 1 in countries array
                    provinceSpinner.setEnabled(true);
                    provinceLabel.setEnabled(true);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) { }
        });

        checkRecordValues();

        if (Globals.COUNTRY.equals("South Africa")){
            provinceSpinner.setEnabled(true);
            provinceLabel.setEnabled(true);
        }
    }

    private void checkRecordValues(){
        if(currentRecord.COUNTRY != null)
            countrySpinner.setSelection(Arrays.binarySearch(countries, currentRecord.COUNTRY));
        if(currentRecord.PROVINCE  != null)
            provinceSpinner.setSelection(Arrays.binarySearch(provinces, currentRecord.PROVINCE));
        if(currentRecord.NEAREST_TOWN != null)
            nearestTownET.setText(currentRecord.NEAREST_TOWN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dbHelper != null) {
            OpenHelperManager.releaseHelper();
            dbHelper = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_done)
         {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            saveDetails();
                            saveToDB();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            currentRecord = null;
                            Intent homeActivity = new Intent(getApplicationContext(), HomeActivity.class);
                            homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
                            startActivity(homeActivity);
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Save Record").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return super.onOptionsItemSelected(item);
    }
    private void onClickBack(){
        saveDetails();
        Intent previousIntent = new Intent(this, EnvironmentDetailsActivity.class);
        previousIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(previousIntent);
    }

    private void saveDetails(){
        currentRecord.COUNTRY = countrySpinner.getSelectedItem().toString();
        currentRecord.PROVINCE = provinceSpinner.getSelectedItem().toString();
        if(nearestTownET.getText().toString().equals(""))
            currentRecord.NEAREST_TOWN = Globals.NA;
        else
            currentRecord.NEAREST_TOWN = nearestTownET.getText().toString();
    }

    private void saveToDB(){
        getDao();
        try {
            recordDao.createOrUpdate(currentRecord);
            showDialog();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getDao(){
        dbHelper = new DatabaseHelper(getApplicationContext());
        dbHelper.getWritableDatabase(); // ###########################
        try {
            recordDao = dbHelper.getRecordDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void showDialog(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(homeIntent);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Record saved successfully").setPositiveButton("OK", dialogClickListener).show();
    }
}
