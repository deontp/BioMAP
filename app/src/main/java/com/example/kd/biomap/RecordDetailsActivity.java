package com.example.kd.biomap;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.Records.Record;

public class RecordDetailsActivity extends AppCompatActivity {

    Record currentRecord;
    private static Spinner projectTypeSpinner;

    private String[] projects;

    private static CheckBox flowersPresentCB, fruitPresentCB, scentCB;
    private static EditText amountSpottedET, scentDescriptionET,identificationTypeET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_details);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.record_details_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Record Details");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }

        currentRecord = getIntent().getParcelableExtra(Record.class.toString());

        projects = getResources().getStringArray(R.array.projects);

        projectTypeSpinner = (Spinner) findViewById(R.id.spinner_project_type);
        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, projects );
        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        projectTypeSpinner.setAdapter(SpinnerAdapter);



        flowersPresentCB = (CheckBox) findViewById(R.id.checkbox_flowers);
        fruitPresentCB = (CheckBox) findViewById(R.id.checkbox_fruit);
        scentCB = (CheckBox) findViewById(R.id.checkbox_scent);
        amountSpottedET = (EditText) findViewById(R.id.editText_amount_spotted);
        scentDescriptionET = (EditText) findViewById(R.id.editText_scent_description);
        identificationTypeET = (EditText) findViewById(R.id.edittext_identification_type);
        checkRecordValues();
        if(scentDescriptionET != null)
            scentDescriptionET.setEnabled(false);

        scentCB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                    scentDescriptionET.setEnabled(((CheckBox) v).isChecked());
            }
        });
    }

    private void checkRecordValues(){
        if(currentRecord.PROJECT_TYPE != null)
            projectTypeSpinner.setSelection(0);// set the spinner to the correct value after getting the information from the shared preferences
        if(currentRecord.IDENTIFICATION_TYPE  != null) {
            identificationTypeET.setText(currentRecord.IDENTIFICATION_TYPE);
        }
        amountSpottedET.setText(currentRecord.AMOUNT_SPOTTED.toString());
        scentCB.setChecked(currentRecord.HAS_SCENT);
        fruitPresentCB.setChecked(currentRecord.HAS_FRUIT);
        flowersPresentCB.setChecked(currentRecord.HAS_FLOWERS);

        if(currentRecord.SCENT_DESCRIPTION != null)
            scentDescriptionET.setText(currentRecord.SCENT_DESCRIPTION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_next)
        {
            saveRecordDetails();
            Intent nextIntent = new Intent(this, EnvironmentDetailsActivity.class);
            nextIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
            startActivity(nextIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClickBack(){
        saveRecordDetails();
        Intent previousIntent = new Intent(this, ReviewPhotosActivity.class);
        previousIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(previousIntent);
    }

    private void saveRecordDetails(){
        currentRecord.AMOUNT_SPOTTED = Integer.parseInt(amountSpottedET.getText().toString());
        currentRecord.HAS_FLOWERS = flowersPresentCB.isChecked();
        currentRecord.HAS_FRUIT = fruitPresentCB.isChecked();
        currentRecord.HAS_SCENT = scentCB.isChecked();
        if(scentDescriptionET.getText().toString().equals(""))
            currentRecord.SCENT_DESCRIPTION = Globals.NA;
        else
            currentRecord.SCENT_DESCRIPTION = scentDescriptionET.getText().toString();
        if(identificationTypeET.getText().toString().equals(""))
            currentRecord.IDENTIFICATION_TYPE = Globals.NA;
        else
            currentRecord.IDENTIFICATION_TYPE = identificationTypeET.getText().toString();
        currentRecord.PROJECT_TYPE = projectTypeSpinner.getSelectedItem().toString();
    }

}
