package com.example.kd.biomap;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import com.example.kd.biomap.Adapters.RecordAdapter;
import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.ORMLlite.DatabaseHelper;
import com.example.kd.biomap.Records.Record;
import com.j256.ormlite.dao.Dao;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ExistingRecordsActivity extends AppCompatActivity {

    ListView listView;
    RecordAdapter adapter;
    private final static int RECORD_UPDATED = 0;
    private final static int RECORD_UPLOADED = 1;
    private static List<Record> records;
    private static ProgressDialog prgDialog;

    private DatabaseHelper dbHelper = null;
    private Dao<Record, Integer> recordDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_records);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.existing_records_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Existing Records");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }

        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);


        getAllRecords();
        listView = (ListView) findViewById(R.id.listView_existing_records);
        adapter = new RecordAdapter(this, (ArrayList<Record>) records );
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailsIntent = new Intent(getApplicationContext(), RecordActivity.class);
                detailsIntent.putExtra(Record.class.toString(), (Parcelable) records.get(position));
                startActivity(detailsIntent);
            }
        });
    }

    private void getAllRecords(){
        getDao();
        try {
            records = recordDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getDao(){
        dbHelper = new DatabaseHelper(getApplicationContext());
        dbHelper.getWritableDatabase(); // ###########################
        try {
            recordDao = dbHelper.getRecordDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menuback_upload_settings, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(this, AboutActivity.class);
            startActivity(aboutIntent);
        }
        if (id == R.id.action_help) {
            Intent helpIntent = new Intent(this, HelpActivity.class);
            startActivity(helpIntent);
        }
        if(id == R.id.action_upload){
            onUploadClicked();
        }
        if (id == R.id.action_logout) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void onClickBack() {
        Intent homeActivity = new Intent(getApplicationContext(), HomeActivity.class);
        homeActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
        startActivity(homeActivity);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode != RESULT_OK)
            return;

        if( requestCode == RECORD_UPDATED){
            adapter.notifyDataSetChanged();
        }
        else if( requestCode == RECORD_UPLOADED){
            // this will change to remove the record from the list
            adapter.notifyDataSetChanged();
        }
    }

    public void onUploadClicked(){
        ArrayList<Record> toDelete = new ArrayList<>();
        for (int a = 0; a < records.size(); a++){
            CheckBox cBox = (CheckBox) listView.getChildAt(a).findViewById(R.id.checkBox_record_selected);

            if(cBox != null){
                if(cBox.isChecked())
                    toDelete.add(records.get(a));
            }
        }
        uploadRecord(toDelete);
    }

    public void uploadRecord(ArrayList<Record> record){
        try {
            for(int i =0; i <record.size(); i++) {
                uploadtoServer(record.get(i));
                recordDao.delete(record.get(i));
                adapter.removeRecords(record);
                //showDialog();
            }
            adapter.removeRecords(record);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }


    public void uploadtoServer(Record record) {
        // catches any exceptions

        String date = record.DATE;
        String[] parts = date.split("-");
        RequestParams params = new RequestParams();
        try {
            String username = record.FULL_NAME;
            String userid = String.valueOf(record.ADU_NUMBER);
            String email = record.EMAIL;
            String project = record.PROJECT_TYPE;
            String observers = "";
            String country = record.COUNTRY;
            String province = record.PROVINCE;
            String nearesttown = record.NEAREST_TOWN;
            String locality = record.NEAREST_TOWN;// need to think of how to do this
            String minelev = null;
            String maxeleve = null;
            String lat = record.LATITUDE;
            String Long = record.LONGITUDE;
            String datum = "WGS84";
            String accuracy = "";
            String source = "Google Map";// dunno how to make this manual?

            //Dunno if this is going to work
            String year = parts[2];
            String month = parts[1];
            String day = parts[0];

            String note = "";//?
            String userdet = record.IDENTIFICATION_TYPE;
            String nestcount = null;
            String nestsite = null;
            String roadkill = null;
            String taxonid = null;
            String taxonname = null;
            String institution_code = null;
            String collection = null;
            String cat_number = null;
            String recordbasis = "VMUS";
            String recordstatus = record.GROWING;
            String imageurl = "";

            if(userid.equals("") || email.equals("") || project.equals(""))
                throw new Exception();
            params.put("username", username);
            params.put("userid", userid);
            params.put("email", email);
            params.put("project", project);
            params.put("observers", observers);
            params.put("country", country);
            params.put("province", province);
            params.put("nearesttown", nearesttown);
            params.put("locality", locality);
            params.put("minelev", minelev);
            params.put("maxelev", maxeleve);
            params.put("lat", lat);
            params.put("long", Long);
            params.put("datum", datum);
            params.put("accuracy", accuracy);
            params.put("source", source);
            params.put("year", year);
            params.put("month", month);
            params.put("day", day);
            params.put("note", note);
            params.put("userdet", userdet);
            params.put("nestcount", nestcount);
            params.put("nestsite", nestsite);
            params.put("roadkill", roadkill);
            params.put("taxonid", taxonid);
            params.put("taxonname", taxonname);
            params.put("institution_code", institution_code);
            params.put("collection", collection);
            params.put("cat_number", cat_number);
            params.put("recordbasis", recordbasis);
            params.put("recordstatus", recordstatus);
            params.put("imageurl", imageurl);
            authenticateUpload(params);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Unfortunately, one of the selected projects has not supplied the necessary details", Toast.LENGTH_LONG).show();
        }
    }

    public Boolean authenticateUpload(RequestParams params) {
        prgDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post("http://vmus.adu.org.za/api/v1/insertrecord", params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                prgDialog.hide();
                Toast.makeText(getApplicationContext(), "Server accepted record " + String.valueOf(statusCode), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String errorResponse, Throwable e) {
                Toast.makeText(getApplicationContext(), "Server did not accept record " + String.valueOf(statusCode), Toast.LENGTH_LONG).show();
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                prgDialog.hide();
            }
            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
        return true;
    }

    private void showDialog(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Record uploaded successfully").setPositiveButton("OK", dialogClickListener).show();
    }

}
