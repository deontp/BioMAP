package com.example.kd.biomap.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.view.View;

import com.example.kd.biomap.Globals.Globals;

import java.util.Locale;

/**
 * Created by deontp on 2016/05/27.
 */
public class SPLoaderService extends IntentService {

    SharedPreferences registrationDetails; // gets the SP file
    SharedPreferences settingsDetails;
    SharedPreferences langsetting;

    public SPLoaderService(){
        super("SPLoaderService");
    }

    public SPLoaderService(String name) {
        super(name);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        registrationDetails = getSharedPreferences(Globals.PREFS_FILE_REGISTRATION_DETAILS, 0);
        settingsDetails = getSharedPreferences( Globals.SETTINGS_PREF, 0); // gets the SP file
        setGlobals();
    }


    private void setGlobals(){
        Globals.COUNTRY = settingsDetails.getString(Globals.SETTINGS_COUNTRY, "South Africa");
        Globals.PROVINCE = settingsDetails.getString(Globals.SETTINGS_PROVINCE, "Eastern Cape");
        Globals.LANGUAGE = settingsDetails.getString(Globals.SETTINGS_LANGUAGE, "English");
        Globals.FULL_NAME = registrationDetails.getString(Globals.PREFS_ENTRY_FULL_NAME, "User");
        Globals.EMAIL_ADDRESS = registrationDetails.getString(Globals.PREFS_ENTRY_EMAIL_ADDRESS, "user@example.co.za");
        Globals.ADU_NUMBER = registrationDetails.getInt(Globals.PREFS_ENTRY_ADU_NUMBER, -1);
        Globals.TOKEN = registrationDetails.getString(Globals.PREFS_ENTRY_TOKEN, null);
    }
}
