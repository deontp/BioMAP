package com.example.kd.biomap;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.loopj.android.http.*;

import com.example.kd.biomap.Globals.Globals;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends AppCompatActivity {

    private static SharedPreferences registrationDetails;
    private static EditText etAduNumber;
    private static EditText etEmail;
    private static EditText etPassword;
    private static ProgressDialog prgDialog;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registrationDetails = getSharedPreferences(Globals.PREFS_FILE_REGISTRATION_DETAILS, 0); // gets the SP file
        etPassword = (EditText) findViewById(R.id.password);
        etEmail = (EditText) findViewById(R.id.email_address);
        etAduNumber = (EditText) findViewById(R.id.adu_number);

        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        // checks to see if the app has a registered user if not shows the log on page, else loads the home page

        Toolbar myToolbar = (Toolbar) findViewById(R.id.register_toolbar_top);
        if (myToolbar != null) {
            myToolbar.setTitle("Login");
            setSupportActionBar(myToolbar);
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // what happens when the menu items are clicked or selected or pressed
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(this, AboutActivity.class);
            startActivity(aboutIntent);
        }
        if (id == R.id.action_help) {
            Intent helpIntent = new Intent(this, HelpActivity.class);
            startActivity(helpIntent);
        }
        return super.onOptionsItemSelected(item);
    }


    // calls the ADU webpage to register the user
    public void onClickRegister(View view) {
        Intent registerIntent = new Intent(Intent.ACTION_VIEW);
        registerIntent.setData(Uri.parse(Globals.ADU_WEBSITE));
        startActivity(registerIntent);
    }

    public void saveDetails(JSONObject userObject) {
        SharedPreferences.Editor editor = registrationDetails.edit();
        try {
            String name = "" + userObject.getJSONObject("registered").getJSONObject("data").get("Name") + " " + userObject.getJSONObject("registered").getJSONObject("data").get("Surname");
            editor.putBoolean(Globals.PREFS_ENTRY_REGISTERED, true);
            editor.putString(Globals.PREFS_ENTRY_FULL_NAME, name);
            editor.putString(Globals.PREFS_ENTRY_EMAIL_ADDRESS, userObject.getJSONObject("registered").getJSONObject("data").get("Email").toString());
            editor.putInt(Globals.PREFS_ENTRY_ADU_NUMBER, Integer.parseInt(userObject.getJSONObject("registered").getJSONObject("data").get("ADUNumber").toString()));
            editor.putString(Globals.PREFS_ENTRY_TOKEN, userObject.getJSONObject("registered").getJSONObject("status").get("token").toString());
            editor.apply();
        }catch (Exception e){

        }

        Intent settingsIntent = new Intent(this, SettingsOnStart.class);
        startActivity(settingsIntent);
    }

    // registered the user and stores the information in SP
    public void onClickLogin(View view) {
        // catches any exceptions
        RequestParams params = new RequestParams();
        try {
            String adu = etAduNumber.getText().toString();
            String email = etEmail.getText().toString();
            String password = etPassword.getText().toString();
            String hashedPassword = MD5Hash(password);
            if(etAduNumber.equals("") || etEmail.equals("") || password.equals(""))
                throw new Exception();
            params.put("userid", adu);
            params.put("email", email);
            params.put("passid", hashedPassword);
            params.put("API_KEY", "e6f43a4ed3770dc3cf20f91aefcffc25");
            authenticateUser(params);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Check details", Toast.LENGTH_LONG).show();
        }
    }

    public String MD5Hash(String s) {
        StringBuffer MD5Hash = new StringBuffer();
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();


            for (int i = 0; i < messageDigest.length; i++)
            {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                MD5Hash.append(h);
            }
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return MD5Hash.toString();
    }

    public Boolean authenticateUser(RequestParams params) {
        prgDialog.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://api.adu.org.za/validation/user/login?", params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                prgDialog.hide();
                try {
                    // When the JSON response has status boolean value assigned with true
                    // String res = (String ) response.getJSONObject("registered").getJSONObject("status").get("result").equals("success")
                    if(response.getJSONObject("registered").getJSONObject("status").get("result").equals("success")){
                        Toast.makeText(getApplicationContext(), "You are successfully logged in!", Toast.LENGTH_LONG).show();
                        // Navigate to Home screen
                        saveDetails(response);
                    }
                    // Else display error message
                    else{
                        Toast.makeText(getApplicationContext(), "Invalid details", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String errorResponse, Throwable e) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Register Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kd.biomap/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Register Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.kd.biomap/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }
}