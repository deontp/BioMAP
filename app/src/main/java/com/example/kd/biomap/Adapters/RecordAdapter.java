package com.example.kd.biomap.Adapters;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kd.biomap.R;
import com.example.kd.biomap.Records.Record;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deontp on 2016/05/25.
 */
public class RecordAdapter extends ArrayAdapter<Record> {

    private final ArrayList<Record> values;

    public RecordAdapter(Context context, ArrayList<Record> values) {
        super(context, 0, values);
        this.values = values;
    }

    public void removeRecords(ArrayList<Record> records){
        values.removeAll(records);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        Record record = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.existing_records_row_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.textView_record_date);
        TextView tvHome = (TextView) convertView.findViewById(R.id.textView_record_time);
        ImageView previewImage = (ImageView) convertView.findViewById(R.id.imageView_record_image);
        CheckBox cb = (CheckBox) convertView.findViewById(R.id.checkBox_record_selected);
        cb.setChecked(false);
        // Populate the data into the template view using the data object
        tvName.setText(record.DATE);
        tvHome.setText(record.TIME);
        if(record.getImageCount() > 0)
            previewImage.setImageURI(Uri.parse(record.IMAGE_PATHS.get(0)));
        else
            previewImage.setImageURI(null);
        // Return the completed view to render on screen
        return convertView;
    }
}
