package com.example.kd.biomap;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.Records.Record;

public class EnvironmentDetailsActivity extends AppCompatActivity {

    private static Spinner environmentTypeSpinner, growingFromSpinner, growingSpinner;
    private static EditText descriptionET;
    Record currentRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment_details);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.environment_details_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Environment Details");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }

        currentRecord = getIntent().getParcelableExtra(Record.class.toString());

        String[] environmentTypeArray = getResources().getStringArray(R.array.environment_type_array);
        String[] growingFromArray = getResources().getStringArray(R.array.growing_from_array);
        String[] growingArray = getResources().getStringArray(R.array.growing_array);

        environmentTypeSpinner = (Spinner) findViewById(R.id.spinner_environment_type);
        growingFromSpinner = (Spinner) findViewById(R.id.spinner_growing_from);
        growingSpinner = (Spinner) findViewById(R.id.spinner_growing);
        descriptionET = (EditText) findViewById(R.id.editText_environment_description);

        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, environmentTypeArray );

        SpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        environmentTypeSpinner.setAdapter(SpinnerAdapter);
        SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, growingFromArray);
        growingFromSpinner.setAdapter(SpinnerAdapter);
        SpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, growingArray);
        growingSpinner.setAdapter(SpinnerAdapter);
        checkRecordValues();

    }

    private void checkRecordValues(){
        if(currentRecord.ENVIRONMENT_TYPE != null)
            environmentTypeSpinner.setSelection(0);// set the spinner to the orrect value after getting the information from the sahred preferences
        if(currentRecord.GROWING_FROM  != null)
            growingFromSpinner.setSelection(0);
        if(currentRecord.GROWING != null)
            growingSpinner.setSelection(0);
        if(currentRecord.ENVIRONMENT_DESCRIPTION != null)
            descriptionET.setText(currentRecord.ENVIRONMENT_DESCRIPTION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_next)
        {
            saveRecordDetails();
            Intent nextIntent = new Intent(this, LocationDetailsActivity.class);
            nextIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
            startActivity(nextIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void onClickBack(){
        saveRecordDetails();
        Intent previousIntent = new Intent(this, RecordDetailsActivity.class);
        previousIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(previousIntent);
    }

    private void saveRecordDetails(){
        currentRecord.ENVIRONMENT_TYPE = environmentTypeSpinner.getSelectedItem().toString();
        currentRecord.GROWING_FROM = growingFromSpinner.getSelectedItem().toString();
        currentRecord.GROWING = growingSpinner.getSelectedItem().toString();
        if(descriptionET.getText().toString().equals(""))
            currentRecord.ENVIRONMENT_DESCRIPTION = Globals.NA;
        else
            currentRecord.ENVIRONMENT_DESCRIPTION = descriptionET.getText().toString();
    }
}

