package com.example.kd.biomap;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kd.biomap.ORMLlite.DatabaseHelper;
import com.example.kd.biomap.Records.Record;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

public class RecordActivity extends AppCompatActivity {

    private static int status;
    Record currentRecord;
    private static TextView tvTime;
    private static TextView tvDate;
    private static TextView tvDescription;
    private static ImageView ivImage;
    private static TextView tvCoordinates;


    private DatabaseHelper dbHelper = null;
    private Dao<Record, Integer> recordDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.record_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Record");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }

        currentRecord = getIntent().getParcelableExtra(Record.class.toString());
        setupDetails();
        status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

    }

    private void SaveRecord(){
        getDao();
        try {
            recordDao.update(currentRecord);
            showDialog();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getDao(){
        dbHelper = new DatabaseHelper(getApplicationContext());
        dbHelper.getWritableDatabase(); // ###########################
        try {
            recordDao = dbHelper.getRecordDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void setupDetails(){
        tvTime = (TextView) findViewById(R.id.textView_record_time);
        tvDate = (TextView) findViewById(R.id.textView_record_date);
        tvDescription = (TextView) findViewById(R.id.textView_record_description);
        ivImage = (ImageView) findViewById(R.id.imageView_record_image);

        //coordinate textView population
        tvCoordinates = (TextView) findViewById(R.id.tv_current_location);
        if(currentRecord.LATITUDE == null || currentRecord.LONGITUDE == null)
        {
            tvCoordinates.setText("No Coordinates Recorded");
        }else {
            tvCoordinates.setText("Latitude: " + currentRecord.LATITUDE + " | " + "Longitude: " + currentRecord.LONGITUDE);
        }
        //Adapt this textView to the coordinates recorded in google maps, or manually. Initiate shared Preference for this
        tvCoordinates = (TextView) findViewById(R.id.current_location_label);

        tvTime.setText(currentRecord.TIME);
        tvDate.setText(currentRecord.DATE);
        tvDescription.setText((currentRecord.ENVIRONMENT_DESCRIPTION));
        if(currentRecord.getImageCount() > 0)
            ivImage.setImageURI(Uri.parse(currentRecord.IMAGE_PATHS.get(0)));
        else
            ivImage.setImageURI(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_done, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_done) {
            SaveRecord();
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClickBack(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent existIntent = new Intent(getApplicationContext(), ExistingRecordsActivity.class);
                        startActivity(existIntent);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("All Changes will be lost.").setPositiveButton("Ok", dialogClickListener)
                .setNegativeButton("Cancel", dialogClickListener).show();
    }
    private void showDialog(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent existIntent = new Intent(getApplicationContext(), ExistingRecordsActivity.class);
                        startActivity(existIntent);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Record saved successfully").setPositiveButton("OK", dialogClickListener).show();
    }

    public void onClickUseOwn(View view){
        Intent manual = new Intent (getApplicationContext(), ManualCoordinatesActivity.class);
        manual.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(manual);
    }

    public  void onClickUseMap(View view){
        Intent mapIntent = new Intent (getApplicationContext(), MapActivity.class);
        mapIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        startActivity(mapIntent);
    }


    public void onClickEditRecord(View view){
        Intent reviewPhotosIntent = new Intent(getApplicationContext(), ReviewPhotosActivity.class);
        reviewPhotosIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        reviewPhotosIntent.putExtra("new_record", false);
        startActivity(reviewPhotosIntent);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}
