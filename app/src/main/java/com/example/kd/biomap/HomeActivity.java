package com.example.kd.biomap;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.Records.Record;
import com.example.kd.biomap.Services.SPLoaderService;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener {

    final static int REQUEST_ACCESS_COARSE_LOCATION = 0;
    final static int REQUEST_ACCESS_FINE_LOCATION = 1;
    final static int GPS_REQUEST = 2;

    private static Location location;
    private static TextView tvCurrentLocation;
    private static GoogleMap googleMap;
    private static LocationManager locationManager;
    private static Boolean languageSet = false;

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-HH-mm", Locale.ENGLISH);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SharedPreferences langsetting = getSharedPreferences(Globals.SETTINGS_PREF, 0);
        if (!languageSet)
            setLanguage(langsetting.getString(Globals.SETTINGS_LANGUAGE, "English"));

        startService(new Intent(this, SPLoaderService.class));
        View view = getCurrentFocus();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.home_toolbar_top); // creates the toolbar
        if (myToolbar != null) {
            myToolbar.setTitle("Home"); // gives it a title
            setSupportActionBar(myToolbar); // sets it as action bar
        }

            String temp = Globals.LANGUAGE;
//        //set language by default based on shared preference NEEDS WORK
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);


        // get a reference to the map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        tvCurrentLocation = (TextView) findViewById(R.id.tv_current_location);

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Getting the name of the best provider
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            buildAlertMessage();
        int currentAPI = Build.VERSION.SDK_INT;
        if (currentAPI >= android.os.Build.VERSION_CODES.M)
            setupLocation23();
        else
            setupLocation();
    }

    private void setLanguage(String language){

        switch (language){
            case "English":
                translateTo("en");
                break;
            case "Afrikaans":
                translateTo("af");
                break;
            case "Xhosa":
                translateTo("xh");
                break;
        }
    }

    public void translateTo(String str) {
        Locale locale = new Locale (str);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());
        languageSet = true;
        recreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_standard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(this, AboutActivity.class);
            startActivity(aboutIntent);
        }
        if (id == R.id.action_help) {
            Intent helpIntent = new Intent(this, HelpActivity.class);
            startActivity(helpIntent);
        }
        if (id == R.id.action_logout) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        try {
            googleMap.setMyLocationEnabled(true);
        } catch (SecurityException e){
            Toast.makeText(this, "Check security permissions", Toast.LENGTH_LONG).show();
        }
        onLocationChanged(location);
//        updateLocation();
    }

    @Override
    public void onLocationChanged(Location location) {
        // Called when a new location is found by the network location provider.
//            updateLocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }


    public void onClickCreateRecord(View view) {
        // create a new record giving it an ID of the date and time
        Intent newRecord = new Intent(this, ReviewPhotosActivity.class);
        newRecord.putExtra(Record.class.toString(), (Parcelable) new Record(sdf.format(new Date())));
        startActivity(newRecord);
    }

    public void onClickViewRecords(View v) {
        Intent existingRecordsIntent = new Intent(this, ExistingRecordsActivity.class);
        startActivity(existingRecordsIntent);
    }

    public void updateLocation() {
        // Getting latitude of the current location
        double currentLatitude = location.getLatitude();
        // Getting longitude of the current location
        double currentLongitude = location.getLongitude();
        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        // Setting latitude and longitude in the TextView tv_location
        String locationString = "Latitude:" +  currentLatitude  + "| Longitude:"+ currentLongitude;
        tvCurrentLocation.setText(locationString);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_ACCESS_COARSE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupLocation();
                return;
            }
        }
        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setupLocation();
                return;
            }
        }
        // if it wasn't the request code I wanted, then call the super
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setupLocation() {
        // get a reference to the map fragment
        tvCurrentLocation = (TextView) findViewById(R.id.tv_current_location);

        // Getting LocationManager object from System Service LOCATION_SERVICE


        String provider = locationManager.getBestProvider(new Criteria(), true);

        // Getting Current Location
        try {
            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider, 20000, 0, this);
        } catch (SecurityException e) {
            Toast.makeText(this, "Check security permissions", Toast.LENGTH_LONG).show();
        }

        if (location != null)
            onLocationChanged(location);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        googleMap = mapFragment.getMap();

    }

    @TargetApi(23)
    private void setupLocation23() {
        if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_ACCESS_COARSE_LOCATION);
            return;
        }
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_ACCESS_FINE_LOCATION);
            return;
        }

        setupLocation();
    }

    private void buildAlertMessage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_REQUEST);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}

