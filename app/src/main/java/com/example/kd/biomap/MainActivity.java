package com.example.kd.biomap;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.kd.biomap.Globals.Globals;
import com.example.kd.biomap.Records.Record;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        try {
//
//            testOutOrmLiteDatabase();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        SharedPreferences registrationDetails = getSharedPreferences(Globals.PREFS_FILE_REGISTRATION_DETAILS, 0); // gets the SP file
        // gets the information from the SP file and stores them in the global variables
        Boolean REGISTERED = registrationDetails.getBoolean(Globals.PREFS_ENTRY_REGISTERED, false);
        registrationDetails = null;
        if (REGISTERED)
        {
            Intent homeIntent = new Intent(this, HomeActivity.class);
            homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
            startActivity(homeIntent);
        }
        else{
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            registerIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.
            startActivity(registerIntent);
        }
    }

//    private void testOutOrmLiteDatabase() throws SQLException {
//        DatabaseHelper DatabaseHelper = OpenHelperManager.getHelper(this,
//                DatabaseHelper.class);
//
//        Dao<Record, Long> RecordDao = DatabaseHelper.getDao();
//
//        Date currDateTime = new Date(System.currentTimeMillis());
//
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(currDateTime);
//        calendar.add(Calendar.DATE, 14);
//
//        //Date dueDate = calendar.getTime();
//
//        //todoDao.create(new Todo("Todo Example 1", "Todo Example 1 Description", currDateTime, dueDate));
//        //todoDao.create(new Todo("Todo Example 2", "Todo Example 2 Description", currDateTime, dueDate));
//        //todoDao.create(new Todo("Todo Example 3", "Todo Example 3 Description", currDateTime, dueDate));
//        RecordDao.create(new Record("Example"));
//        List<Record> todos = RecordDao.queryForAll();
//    }
}
