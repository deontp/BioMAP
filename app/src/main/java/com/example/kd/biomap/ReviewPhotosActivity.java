package com.example.kd.biomap;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kd.biomap.ORMLlite.DatabaseHelper;
import com.example.kd.biomap.Records.Record;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;

import java.sql.SQLException;


public class ReviewPhotosActivity extends AppCompatActivity {

    //the images to display
    private Record currentRecord;
    private static final int GET_CAMERA_PICTURE_RESULT = 0;
    private static final int GET_GALLERY_PICTURE_RESULT = 1;
    private static final int PERMISSIONS_ACCESS_CAMERA = 2;
    private static final int PERMISSIONS_WRITE_EXTERNAL_STORAGE = 3;
    private static final int PERMISSIONS_READ_EXTERNAL_STORAGE = 4;
    private static final int CAMERA = 5;
    private static final int STORAGE = 6;

    private static ImageView previewImage;
    private static ImageView addImageButton;
    private static TextView removeButton;
    private static LinearLayout imageLayout;
    private static ImageView[] imageViews;
    private static Boolean newRecord;

    private static int clickedIndex;

    Matrix matrix = new Matrix();
    private static DatabaseHelper dbHelper;
    private static Dao<Record, Integer> recordDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_photos);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.review_photos_toolbar_top);
        if(myToolbar != null) {
            myToolbar.setTitle("Record");
            setSupportActionBar(myToolbar);

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    onClickBack();
                }
            });
        }



        // get the record that was sent with this Intent
        currentRecord = getIntent().getParcelableExtra(Record.class.toString());
        newRecord = getIntent().getBooleanExtra("new_record", true);
        removeButton = (TextView) findViewById(R.id.button_remove);
        addImageButton = (ImageView) findViewById(R.id.imageView_camera);
        previewImage = (ImageView) findViewById(R.id.imageView_selected_image);
        imageLayout = (LinearLayout) findViewById(R.id.linerLayout_image_preview);
        matrix.postRotate(90);
        imageViews = new ImageView[]{(ImageView) findViewById(R.id.tableRow_image_1), (ImageView) findViewById(R.id.tableRow_image_2), (ImageView) findViewById(R.id.tableRow_image_3)};


        if(imageLayout != null) {
            imageLayout.setBackgroundColor(Color.BLACK);
        }

        if(addImageButton != null) {
            addImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(currentRecord.getImageCount() == 3)
                        Toast.makeText(getApplicationContext(), "Limit reached\nRemove one to continue", Toast.LENGTH_LONG).show();
                    else
                        getImage();
                }
            });
        }

        if(currentRecord.getImageCount() == 0) {
            removeButton.setEnabled(false);
            getImage();
        }
        else
            populateImages();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings_standard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
        }
        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(this, AboutActivity.class);
            startActivity(aboutIntent);
        }
        if (id == R.id.action_help) {
            Intent helpIntent = new Intent(this, HelpActivity.class);
            startActivity(helpIntent);
        }
        if (id == R.id.action_logout) {
            Intent registerIntent = new Intent(this, RegisterActivity.class);
            startActivity(registerIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case GET_CAMERA_PICTURE_RESULT :
                if(resultCode == RESULT_OK){
                    addImage(imageReturnedIntent.getData());
                }
                break;
            case GET_GALLERY_PICTURE_RESULT:
                if(resultCode == RESULT_OK && imageReturnedIntent != null){
                    addImage(imageReturnedIntent.getData());
                }
                break;
        }
    }

    public void onClickDone(View v) {
        final Intent recordIntent = new Intent(getApplicationContext(), RecordDetailsActivity.class);
        recordIntent.putExtra(Record.class.toString(), (Parcelable) currentRecord);
        if (newRecord) {
            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setMessage("Do you want to edit the record now or save to edit later?")
                    .setCancelable(true)
                    .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(recordIntent);
                        }
                    })
                    .setNegativeButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            saveToDB();
                        }
                    });
            final android.app.AlertDialog alert = builder.create();
            alert.show();
        } else {
            startActivity(recordIntent);
        }
    }


    public void onClickBack(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            Intent backIntent;
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        currentRecord = null;
                        if(newRecord){
                            backIntent = new Intent(getApplicationContext(), HomeActivity.class);
                            backIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // clears the stack so back will close the app.

                        }
                        else
                            backIntent = new Intent(getApplicationContext(), ExistingRecordsActivity.class);
                        startActivity(backIntent);
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Changes will be lost.").setPositiveButton("Ok", dialogClickListener)
                .setNegativeButton("Cancel", dialogClickListener).show();
    }


    public void getImage(){
        final CharSequence[] items = { "Camera", "Library"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ReviewPhotosActivity.this);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Camera")) {
                    if(Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                        getImageFromSourceFor23(CAMERA);
                    else
                        getImageFromSource(CAMERA);
                }
                else if (items[item].equals("Library")) {
                    if(Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
                        getImageFromSourceFor23(STORAGE);
                    else
                        getImageFromSource(STORAGE);
                }
            }
        });
        builder.show();
    }

    @TargetApi(23)
    public void getImageFromSourceFor23(Integer source){
        switch (source){
            case CAMERA:
                if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSIONS_ACCESS_CAMERA);
                    return;
                }
                getImageFromSource(CAMERA);
                break;
            case STORAGE:
                if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_READ_EXTERNAL_STORAGE);
                    return;
                }
                getImageFromSource(STORAGE);
                break;
        }
    }
    public void getImageFromSource(Integer source){
        switch (source){
            case CAMERA:
                Intent getCameraPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(getCameraPicture, GET_CAMERA_PICTURE_RESULT);
                break;
            case STORAGE:
                Intent getGalleryPicture = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(getGalleryPicture, GET_GALLERY_PICTURE_RESULT);
                break;
        }
    }

    public void addImage(Uri path){
        currentRecord.addImage(path.toString());
        populateImages();
        removeButton.setEnabled(true);
    }

    private void clearImageViews(){
        for (ImageView a:imageViews) {
            a.setImageURI(null);
        }
    }

    private void populateImages() {
        clearImageViews();
        previewImage.setImageURI(null);
        for (int a = 0; a < currentRecord.IMAGE_PATHS.size(); a++) {
            String temp =currentRecord.IMAGE_PATHS.get(a).trim();
            imageViews[a].setImageURI(Uri.parse(temp));
        }
    }
    private int whichImage(Integer x){
        switch (x){
            case R.id.tableRow_image_1:
                return 0;
            case R.id.tableRow_image_2:
                return 1;
            case R.id.tableRow_image_3:
                return 2;
            default:
                return -1;
        }
    }

    public void onClickPreviewImage (View v){
        clickedIndex = v.getId();
        int whichImage = whichImage(clickedIndex);
        if(whichImage < currentRecord.getImageCount()) {
            imageLayout.setBackgroundColor(Color.WHITE);
            String temp = currentRecord.IMAGE_PATHS.get(whichImage).trim();
            previewImage.setImageURI(Uri.parse(temp));
        }
        else
            previewImage.setImageURI(null);
    }

    public void onClickRemove(View v){
        int index = whichImage(clickedIndex);
        if(index == -1)
            Toast.makeText(this, "Please select an image to remove", Toast.LENGTH_LONG).show();
        else {
            currentRecord.IMAGE_PATHS.remove(index);
            populateImages();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_ACCESS_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromSource(CAMERA);
                return;
            }
        }
        if(requestCode == PERMISSIONS_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromSource(STORAGE);
                return;
            }
        }
        // if it wasn't the request code I wanted, then call the super
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void saveToDB(){
        getDao();
        try {
            recordDao.createOrUpdate(currentRecord);
            showDialog();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getDao(){
        dbHelper = new DatabaseHelper(this);
        dbHelper.getWritableDatabase();
        try {
            recordDao = dbHelper.getRecordDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
    private void showDialog(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        finish();
                        break;
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Record saved successfully").setPositiveButton("OK", dialogClickListener).show();
    }
}
